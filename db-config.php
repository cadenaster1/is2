<?php

class Db{

	public static $conn;

	public static function do_connection( $servername, $username, $password, $dbName ){

		try {
		    $conn = new PDO("mysql:host=" . $servername . ";dbname=" . $dbName, $username, $password);
		    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    //echo "Connected successfully"; 

		    DB::$conn = $conn;

		}
		catch(PDOException $e){
		    echo "Connection failed: " . $e->getMessage();
		}

	}

}