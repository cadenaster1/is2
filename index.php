<?php 

require_once 'config.php';

?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="assets/css/custom.css" />
</head>
<body>

<div class="container-full block p-2">
	<div class="row">
		<div class="col-md-3">
			<?php if( isset( $_SESSION[ "user" ] ) ){ ?>
				<ul>
					<li><a href="?">Home</a></li>
					<li><a href="?c=session&a=logout">Logout</a></li>
				</ul>
			<?php } ?>	
		</div>	

		<div class="col-md-9">
		<?php 

			/* VIEW CALL */
			if( $folder && $file )
				require_once( "mvc/views/" . $folder . "/view-" . $file . ".php" );


		?>
		</div>
	</div>	
</div>
	<script>
		let site_path = "<?php echo SITE_PATH ?>";
	</script>
	<script src="assets/js/jquery-3.3.1.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/custom.js"></script>
</body>
</html>
<?php ?>