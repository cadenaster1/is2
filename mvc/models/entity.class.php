<?php

class Model{

	public static function select( $table, $where = array() ){

		$whereTXT = "1";
		foreach ($where as $key => $value) {
			
			$whereTXT .= " AND " . $value[0] . " " . $value[1] . " '" . $value[2] . "'";

		}

		$result = DB::$conn->query("SELECT * FROM " . $table . " WHERE " . $whereTXT )->fetchAll();

		$return = array();
		foreach ($result as $row) {

			$return[] = $row;

		}

		return $return;

	}

	public static function insert( $table, $values = array() ){

		if( !empty( $values ) ){

			$columns = implode(", ", array_keys( $values ));
			$params = implode(", :", array_keys( $values ));
			
			$query = DB::$conn->prepare( "INSERT INTO " . $table . " (" . $columns . ") VALUES (:" . $params. ")" );

			foreach ($values as $key => $value){
				$query->bindParam( ":" . $key, $key );
			}

			$query->execute( $values );

		}

	}

}