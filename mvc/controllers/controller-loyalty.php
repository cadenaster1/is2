<?php

class loyalty{
	
	public function search_process(){

		$search = isset( $_POST[ "search" ] ) && $_POST[ "search" ] ? addslashes( $_POST[ "search" ] ) : "";

		if( $search ){

			$identification = Model::select( "user", array( array( "identification", "=", $search ) ) );

			if( !empty( $identification ) ){
				$buys = Model::select( "buy", array( array( "user_id", "=", $identification[0][ "id" ] ) ) );
				echo json_encode( array( "user" => $identification[0], "buys" => $buys ) );
			
			}else{

				echo json_encode( array( "user" => "not_found" ) );

			}

		}

		exit();

	}

	public function buy_process(){

		$client_id = isset( $_POST[ "client_id" ] ) && $_POST[ "client_id" ] ? addslashes( $_POST[ "client_id" ] ) : "";
		$total_buy = isset( $_POST[ "total_buy" ] ) && $_POST[ "total_buy" ] ? addslashes( $_POST[ "total_buy" ] ) : "";

		$redenption = isset( $_POST[ "redenption" ] ) && $_POST[ "redenption" ] ? addslashes( $_POST[ "redenption" ] ) : "";

		$buy_type = 1;
		if( $redenption == 1 ){
			$buy_type = 2;
		}

		if( $client_id && $total_buy ){

			Model::insert( "buy", array( "user_id" => $client_id, "buy_type" => $buy_type, "price" => $total_buy, "time" => date( "Y-m-d H:i:s" ) ) );
			echo json_encode( array( "success" => 1 ) );
		}else{
			echo json_encode( array( "success" => 0 ) );
		}

		exit();

	}

}