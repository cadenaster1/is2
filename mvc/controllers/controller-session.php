<?php

class session{

	public function login(){

		return array(
			"view" => "session.login",
			"data" => array( )
		);

	}

	public function login_process(){

		$user = isset( $_POST[ "user" ] ) && $_POST[ "user" ] ? addslashes( $_POST[ "user" ] ) : "";
		$pass = isset( $_POST[ "pass" ] ) && $_POST[ "pass" ] ? addslashes( $_POST[ "pass" ] ) : "";

		if( $user && $pass ){

			$user = Model::select( "user", array( array( "user", "=", $user ), array( "password", "=", md5( $pass ) ) ) );

			if( !empty( $user ) ){
				$_SESSION[ "user" ] = json_encode( $user[0] );
				echo json_encode( array( "login" => 1 ) );
			}else{
				echo json_encode( array( "login" => 0 ) );
			}

		}

		exit();
	}

	public function logout(){

		unset( $_SESSION[ "user" ] );
		header( "Location: " . SITE_PATH );
		exit();

	}

	public function register_process(){

		$fname = isset( $_POST[ "fname" ] ) && $_POST[ "fname" ] ? addslashes( $_POST[ "fname" ] ) : "";
		$lname = isset( $_POST[ "lname" ] ) && $_POST[ "lname" ] ? addslashes( $_POST[ "lname" ] ) : "";
		$identification = isset( $_POST[ "identification" ] ) && $_POST[ "identification" ] ? addslashes( $_POST[ "identification" ] ) : "";

		Model::insert( "user", array( "user_fname" => $fname, "user_lname" => $lname, "identification" => $identification, "user" => "", "password" => "" ) );

		echo json_encode( array( "success" => "1" ) );
		exit();

	}

}