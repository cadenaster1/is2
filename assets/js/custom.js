$( document ).ready( function(){

	$( "#login_form" ).on( "submit", function(){

		$.post( "?c=session&a=login_process", { user: $( "#user_name" ).val(), pass: $( "#user_pass" ).val() }, function(data){

			if( data.login == 1 ){
				location.reload();
			}
			else{
				$( "#user_name, #user_pass" ).val( "" );
				alert( "Datos invalidos" );
			}

		}, "json" );

		return false;

	} )

	$( "#search_form" ).on( "submit", function(){

		refreshData();

		return false;
	} )

	$( "#submit_buy" ).on( "click", function(){

		$.post( site_path + "?c=loyalty&a=buy_process", { client_id: $( "#client_id" ).val(), total_buy: $( "#total_buy" ).val() }, function(){
			refreshData();
		} )

	} )

	$( "#submit_redemption" ).on( "click", function(){

		$.post( site_path + "?c=loyalty&a=buy_process", { client_id: $( "#client_id" ).val(), total_buy: $( "#total_buy" ).val(), redenption: 1 }, function(){
			refreshData();
		} )

	} )

	$( "#register_form" ).on( "submit", function(){

		$.post( site_path + "?c=session&a=register_process", { fname: $( "#fname" ).val(), lname: $( "#lname" ).val(), identification: $( "#identification" ).val() }, function(){
			$( "#input_search" ).val( $( "#identification" ).val() );
			refreshData();

			$( ".with_results" ).css( "display", "block" );
			$( ".register_form_content" ).css( "display", "none" );
		} )

		return false;
	} )

	function refreshData(){

		$.post( site_path + "?c=loyalty&a=search_process", { search: $( "#input_search" ).val() }, function(data){

			if( data.user != "not_found" ){

				$( "#client_id" ).val( data.user.id );
				$( ".user_data" ).html( "Cliente: " + data.user.user_fname + " " + data.user.user_lname );

				$( ".client_transactions" ).html( "" );

				let able_redenption = 0;
				$.each( data.buys, function( index, value ){

					let t_type = ( value.buy_type == 1 ) ? "Compra" : "Redencion";

					if( value.buy_type == 1 ){
						able_redenption += ( value.price * 1 );
					}else{
						able_redenption -= ( value.price * 1 );
					}

					$( ".client_transactions" ).append( "<div class='tr_type_" + value.buy_type + "'>" + t_type + ": " + value.price + " " + value.time + "</div>" );

				} )

				if( able_redenption < 30000 ){
					$( "#submit_redemption" ).css( "display", "none" );
				}else{
					$( "#submit_redemption" ).css( "display", "block" );
				}

				$( ".client_transactions" ).append( "<br /><div>Disponible para redencion: <span class='able_redenption'>" + able_redenption + "</span></div>" );

				$( ".with_results" ).css( "display", "block" );

			}else{
				$( ".with_results" ).css( "display", "none" );
				$( ".register_form_content" ).css( "display", "block" );

			}

		}, "json" )

	}

} )