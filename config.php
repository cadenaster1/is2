<?php

session_start();

define("SITE_PATH", "/is2/");

$c = isset( $_GET[ "c" ] ) && $_GET[ "c" ] ? addslashes( $_GET[ "c" ] ) : "index";
$a = isset( $_GET[ "a" ] ) && $_GET[ "a" ] ? addslashes( $_GET[ "a" ] ) : "search";

if( isset( $_GET[ "debug" ] ) && $_GET[ "debug" ] || true ){
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
}

/* CONTROLLER CALL */
$servername = "localhost";
$username = "cadenaster";
$password = "cadenaster";
$dbName = "is2";

require_once 'db-config.php';
Db::do_connection( $servername, $username, $password, $dbName );

if( !isset( $_SESSION[ "user" ] ) && $a != "login_process" ){
	$c = "session";
	$a = "login";
}

/* General Model */
require_once(  'mvc/models/entity.class.php' );

/* CONTROLLER CALL */
require_once(  'mvc/controllers/controller-' . $c . ".php" );

$obj = new $c;
$array = $obj->$a();

$view = explode( ".", $array[ "view" ] );
$folder = isset( $view[ 0 ] ) ? $view[ 0 ] : 0;
$file = isset( $view[ 1 ] ) ? $view[ 1 ] : 0;

$data = isset( $array[ "data" ] ) ? $array[ "data" ] : array();

if($data){
	foreach ($data as $key => $value) {
		$$key = $value;
	}
}