Test Andres Cadena Tambourine
=============================


Instalation
=============================

1. Create MySQL database
2. Import /testtambourine.sql file to your database
3. Configure connection vars at /config.php line 8

Use
=============================

1. At the browser entro to your server and test the scripts
	- Form saving data by post at database
	- Get data from database with ajax
	- Play Rock, Paper Scissors
	- Stop gninnipS My sdroW!Scissors

	The scripts are at the controllers files: /mvc/controllers